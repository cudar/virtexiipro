# VirtexIIpro

Repositorio creado por estudiantes de la [UTN-FRC](https://www.frc.utn.edu.ar/) en colaboración con el [CUDAR](https://www.instagram.com/cudar.utn/) (Centro universitario de desarrollo en automoción y robótica), con el fin de plasmar la información adquirida sobre la fpga [VirtexIIpro](https://digilent.com/reference/programmable-logic/virtex-ii-pro/start), facilitando así la inicialización con la placa mencionada.  

## Contenido 

### wiki
En la wiki se encuentran algunos tutoriales, realizados con el fin de dar los primeros pasos para poner en marcha la fpga y desarrollar algunos ejemplos con la misma. Los tutoriales son: 
1. [Tutorial VirtualBox e ISE](https://gitlab.com/cudar/virtexiipro/-/wikis/Tutorial-VirtualBox-e-ISE)
1. [Primer proyecto Verilog en ISE 10](https://gitlab.com/cudar/virtexiipro/-/wikis/Primer-proyecto-Verilog-en-ISE-10)
1. [Primer proyecto con XPS](https://gitlab.com/cudar/virtexiipro/-/wikis/Primer-proyecto-con-XPS)

<br>**Nota:** Se recomienda seguir el orden explicito arriba para avanzar de manera gradual.  

### doc 
En este [directorio](https://gitlab.com/cudar/virtexiipro/-/tree/main/doc?ref_type=heads) se encuentran pdf's y enlaces a páginas externas con información relevante, los cuales se utilizaron para profundizar y realizar avances sobre la placa. 

### src 
Dentro de este [directorio](https://gitlab.com/cudar/virtexiipro/-/tree/main/src?ref_type=heads) se encuentra el contenido del CD original de la fpga.

### issues 

En esta [sección](https://gitlab.com/cudar/virtexiipro/-/issues/?sort=created_date&state=all&first_page_size=20) se presentan los inconvenientes/problemas que se presentaron durante el avance con la placa.

## Colaboradores

### Estudiantes 
- Aliendo, Ulises
- Tolaba, Bruno Ignacio

### Docente
- Gutiérrez, Francisco Guillermo
